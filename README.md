## qs-rs

```rust
let x = 1;
let s = stringify!(
    "a" => x,
    "b" => 2,
    "c" => 3,
    "d" => "name age"
);
assert_eq!(s,  "a=1&b=2&c=3&d=name+age");
let mut m = BTreeMap::new();
m.insert("a", 1);
m.insert("b", 2);
m.insert("c", 3);
let s = stringify!(m);
assert_eq!(s,  "a=1&b=2&c=3");
let s = stringify![
    ("a", 1),
    ("b", 2),
    ("c", 3)
];
assert_eq!(s,  "a=1&b=2&c=3");
```