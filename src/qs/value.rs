#[derive(Debug, PartialEq, Clone)]
pub enum QueryValue {
    Integer(i128),
    Float(f64),
    String(String),
    Array(Vec<String>),
    Null,
}

pub trait ToQueryValue {
    fn to(self) -> QueryValue;
}

impl ToQueryValue for i8 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for i16 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for i32 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for i64 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for i128 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self)
    }
}

impl ToQueryValue for u8 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for u16 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for u32 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for u64 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for u128 {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for f32 {
    fn to(self) -> QueryValue {
        QueryValue::Float(self as f64)
    }
}

impl ToQueryValue for f64 {
    fn to(self) -> QueryValue {
        QueryValue::Float(self)
    }
}

impl ToQueryValue for usize {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for isize {
    fn to(self) -> QueryValue {
        QueryValue::Integer(self as i128)
    }
}

impl ToQueryValue for &str {
    fn to(self) -> QueryValue {
        QueryValue::String(self.to_string())
    }
}

impl ToQueryValue for String {
    fn to(self) -> QueryValue {
        QueryValue::String(self)
    }
}

impl<T: ToString> ToQueryValue for Vec<T> {
    fn to(self) -> QueryValue {
        QueryValue::Array(self.into_iter().map(|v| v.to_string()).collect::<Vec<String>>())
    }
}

impl ToQueryValue for QueryValue {
    fn to(self) -> QueryValue {
        self
    }
}
