pub trait UrlEncoding {
    fn url_encode(&self) -> String;
}

impl UrlEncoding for i8 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for i16 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for i32 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for i64 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for u8 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for u16 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for u32 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for u64 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for u128 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for f32 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for f64 { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for isize { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for bool { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for usize { 
    fn url_encode(&self) -> String {
        self.to_string()
    }
}
impl UrlEncoding for &str { 
    fn url_encode(&self) -> String {
        urlencoding::encode(self).to_string()
    }
}
impl UrlEncoding for String { 
    fn url_encode(&self) -> String {
        urlencoding::encode(self.as_str()).to_string()
    }
}